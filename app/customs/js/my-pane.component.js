angular.module("tendon")
	.directive('myPane', function() {
	  return {
	    require: '^^myTabs',
	    restrict: 'E',
	    transclude: true,
	    scope: {
	      title: '@',
	      iconCls: '@'
	    },
	    link: function(scope, element, attrs, tabsCtrl) {
	      tabsCtrl.addPane(scope);
	    },
	    templateUrl: 'client/customs/tpls/my-pane.ng.html'
	  };
	});