'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'uiGmapgoogle-maps',
  'myApp.view1',
  'myApp.view2',
  'myApp.view3',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/home'});
}])
.run(['$rootScope', '$timeout', function($rootScope, $timeout) {
    $rootScope.$on('$viewContentLoaded', function() {
        $timeout(function(){
            componentHandler.upgradeAllRegistered();
        },500);
        document.getElementById('top').scrollIntoView();
        
    });
}])
;
